import { Observable } from 'rxjs/Rx';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Course } from '../model/cource-model';
import { FormArray, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userDataForm: FormGroup;

  userNameList = ['Lara','Sachin','Rohit']

 constructor() {

   }
   
  ngOnInit() {
    this.userDataForm = new FormGroup({

      'userData': new FormGroup({
      'username': new FormControl(null, [Validators.required, this.validateUserName.bind(this)]),
      'email': new FormControl(null, [Validators.required, Validators.email],this.asynchValidateUserName.bind(this)),
      
    }),
      'questions': new FormControl('school'),
      'hobbies': new FormArray([])
    });
  }
  submitUserDataForm(){
    console.log(this.userDataForm);

  }

  addHobby(){
    (<FormArray>this.userDataForm.get('hobbies')).push(
      new FormControl(null)
    )
    console.log(this.userDataForm)
  }

  validateUserName(userNameControl: FormControl):{[key:string]:boolean}{
    if(this.userNameList.indexOf(userNameControl.value) !== -1){
      return {'userNameIsAvailable': true}
    }
   return null;
 }
 asynchValidateUserName(userNameControl : FormControl) : Promise<any> | Observable<any>{
  const promise = new Promise<any>(
    (resolve,reject) => {
      setTimeout(
        () => {
          if(userNameControl.value === "test@test.com"){
            resolve({'emailIsAvailable':true})
          }else{
            resolve(null)
          }
        },5000
      )
    }
  );
  return promise;
}


}
