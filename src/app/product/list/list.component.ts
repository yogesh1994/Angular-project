import { Component, OnInit } from '@angular/core';
import { Course } from '../../model/cource-model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  courses: Array<Course> = [];
  
  constructor(private productService: ProductService) {
    this.courses = productService.courses;
  }

  ngOnInit() {
  }

}
