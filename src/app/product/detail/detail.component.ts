import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Course } from '../../model/cource-model';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  id: number;
  name: string;
  faculty: string;
  course: Course;
  
  constructor(private activatedRoutes: ActivatedRoute,
    private productService: ProductService) {
    this.id = this.activatedRoutes.snapshot.params['id'];
    this.name = this.activatedRoutes.snapshot.queryParams['name'];
    this.faculty = this.activatedRoutes.snapshot.queryParams['faculty'];

    this.getCourseById(this.id);
   }

   getCourseById(id){
     this.course = this.productService.getCourseById(id);
   }
  ngOnInit() {
  }

}
