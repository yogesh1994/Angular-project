import { UtilModule } from '../util/util.module';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { CreateComponent } from './create/create.component';
import { MatCardModule, MatGridListModule, MatInputModule, MatButtonModule } from '@angular/material';
import { ProductService } from './product.service';
import {MatExpansionModule} from '@angular/material/expansion';
import { CourseFormComponent } from './create/course-form/course-form.component';
import { DisplayCourseComponent } from './create/display-course/display-course.component';

const routes: Routes = [
  {
    path: "",
    component: ProductComponent,
    children: [
      {
        path:"", redirectTo: "list", pathMatch: "full"
      },
      {
        path:"create", component: CreateComponent
      },
      {
        path:"list", component: ListComponent
      },
      {
        path:"detail/:id", component: DetailComponent
      }    
    ]
  }
/*  {
    path:"", redirectTo: "list", pathMatch: "full"
  },
  {
    path:"create", component: CreateComponent
  },
  {
    path:"list", component: ListComponent
  },
  {
    path:"detail/:id", component: DetailComponent
  }*/
]

@NgModule({
  imports: [
    CommonModule,MatCardModule,MatGridListModule,FormsModule,RouterModule.forChild(routes),
    MatInputModule,MatButtonModule,MatExpansionModule,UtilModule
  ],
  declarations: [ProductComponent, ListComponent, DetailComponent, CreateComponent, CourseFormComponent, DisplayCourseComponent],
  providers: [ProductService]
})
export class ProductModule { }
