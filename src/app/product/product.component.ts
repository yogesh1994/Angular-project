import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToCreatePage(){
    // let id = 1;
    // this.router.navigate(['/product/create/' + id],{queryParams:{name: 'java'}});
   this.router.navigate(['/product/create']);
  }
}
