import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  EventEmitter,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { Course } from '../../../model/cource-model';

@Component({
selector: 'app-course-form',
templateUrl: './course-form.component.html',
styleUrls: ['./course-form.component.css'],
encapsulation: ViewEncapsulation.Emulated
})
export class CourseFormComponent implements OnInit {

newCourse: Course;

id: number;
name: string;
imgPath: string;

@Output() getCourseData: EventEmitter<Course> = new EventEmitter();
constructor() {
  console.log("constructor called!!")
 }

ngOnInit() {
  console.log("ngOnInit called!!")
}
ngDoCheck(): void {
   console.log("ngDoCheck Called!!!")
}

ngAfterContentInit(){
  console.log("ngAfterContentInit Called!!!")
}

ngAfterContentChecked(){
  console.log("ngAfterContentChecked Called!!!")
}

ngAfterViewInit(){
  console.log("ngAfterViewInit Called!!!")
}

ngAfterViewChecked(){
  console.log("ngAfterViewChecked Called!!!")
}

ngOnDestroy(){
  console.log("ngOnDestroy Called!!!")
}

createCourse(){
  this.newCourse = new Course(this.id, this.name, this.imgPath);
  console.log("new course is " + this.newCourse)
  console.log(this.newCourse)
  this.getCourseData.emit(this.newCourse);
}

}
