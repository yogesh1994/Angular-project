import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Course } from '../../../model/cource-model';

@Component({
  selector: 'app-display-course',
  templateUrl: './display-course.component.html',
  styleUrls: ['./display-course.component.css']
})
export class DisplayCourseComponent implements OnInit {
@Input("coursesData") childCourses : Course[]; 
  constructor() { }

  ngOnInit() { 
  }
  ngOnChanges(changes : SimpleChanges){
    console.log("Display Courses changes are ")
    console.log(changes)
  }

}
