import { AlertComponent } from '../../util/alert/alert.component';
import { eventNames } from '@angular/language-service/src/html_info';
import { ProductService } from '../product.service';
import { Course } from '../../model/cource-model';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {


  //newCourse: Course;
  courses: Course[] = [];
  //id: number;
 // name: string;
  //imgPath: string;
@ViewChild(AlertComponent) alertComponent : AlertComponent;

  constructor(private productService: ProductService) {
    this.getCourses();
   }

  ngOnInit() {
  }

  createCourse(event){
    console.log("Inside  parent and event is")
    console.log(event)
    this.productService.createCourse(event);

   this.alertComponent.successAlert("Courses is successfully added");
    this.getCourses();
  }

  getCourses(){
    this.courses =this.productService.getCourses();

  }
  getFacultyName(faculty){
    console.log("faculty is");
    console.log(faculty);
  }
}
