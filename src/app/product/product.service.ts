import { Injectable } from '@angular/core';
import { Course } from '../model/cource-model';

@Injectable()
export class ProductService {

  courses: Array<Course> = [];
  constructor() {
    let c1: Course =  new Course(1,"java","assets/images/courses/java.png");
    let c2: Course =  new Course(2,"Spring MVC","assets/images/courses/spring.png");
    let c3: Course =  new Course(3,"Angular","assets/images/courses/angular.png");
    let c4: Course =  new Course(4,"MongoDb","assets/images/courses/mongodb.jpg");
    let c5: Course =  new Course(5,"Hibernate","assets/images/courses/hibernate.png");
    
    this.courses.push(c1);
    this.courses.push(c2);
    this.courses.push(c3);
    this.courses.push(c4);
    this.courses.push(c5);
   }

   getCourses(){
     return this.courses;
   }

   createCourse(course: Course){
     this.courses.push(course);
     console.log("new courses are");
     console.log(this.courses)
   }

   getCourseById(id: number){
     for(var i = 0; i< this.courses.length;i++)
     {
       if(this.courses[i].id == id)
       {
         return this.courses[i];
       }
     }
     return null;
   }
}
