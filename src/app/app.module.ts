import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { CoreModule } from './core/core.module';
import {MatIconModule} from '@angular/material/icon';
import { MatSidenavModule, MatButtonModule } from '@angular/material';
import { HomeModule } from './home/home.module';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AboutUsComponent } from './about-us/about-us.component';

const routes: Routes = [
  {path: '', redirectTo : 'home', pathMatch: 'full'},
  {path : 'home', component: HomeComponent},
  {path : 'product', loadChildren: 'app/product/product.module#ProductModule'},
  {path : 'contact-us', loadChildren: 'app/contact-us/contact-us.module#ContactUsModule'},
  {path : 'about-us', loadChildren: 'app/about-us/about-us.module#AboutUsModule'},
  {path : '**', component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    FormsModule,
    CommonModule,
    MatIconModule,
    HomeModule,
    MatSidenavModule,
    MatButtonModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
