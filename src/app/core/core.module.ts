import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { MatSidenavModule, MatToolbarModule, MatListModule, MatButtonModule } from '@angular/material';
import {MatIconModule} from '@angular/material/icon';
import { RightMenuComponent } from './header/right-menu/right-menu.component';

@NgModule({
  imports: [
    CommonModule,MatSidenavModule,MatToolbarModule,MatIconModule,MatListModule,MatButtonModule,
    RouterModule
  ],
  declarations: [FooterComponent, HeaderComponent, RightMenuComponent],
  exports: [FooterComponent, HeaderComponent]
})
export class CoreModule { }
