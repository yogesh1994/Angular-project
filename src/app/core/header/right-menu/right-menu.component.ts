import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-right-menu',
  templateUrl: './right-menu.component.html',
  styleUrls: ['./right-menu.component.css']
})
export class RightMenuComponent implements OnInit {

  @Input() childMenus: any;
  constructor() { }

  ngOnInit() {
  }

}
