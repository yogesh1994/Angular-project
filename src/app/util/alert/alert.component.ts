import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  successAlert(msg:string){
    alert("success msg is "+msg);
  }
  errorAlert(msg:string){
    alert("error msg is "+msg);
  }

}
